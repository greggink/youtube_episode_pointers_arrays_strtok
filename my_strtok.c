#include <string.h>
#include <stdio.h>

char* my_strtok(char *str, char c){

	static char *buffer = NULL;

	if(str){
		buffer = str;
	}
	if(!buffer || !*buffer){
		return NULL;
	}

	char *token = buffer;

	while(*buffer && *buffer != c){
		buffer += 1;
	}
	if(*buffer){
		*buffer = 0;
		buffer += 1;
	}

	return token;

}//my_strtok*/

int main(){

	char str[80] = "This is a test.";
	char *token = 0;

	token = my_strtok(str, ' ');
	while(token){
		printf("%s\n", token);
		token = my_strtok(NULL, ' ');
	}

	//printf("Str is now %s\n", str);

	return 0;

}//main*/