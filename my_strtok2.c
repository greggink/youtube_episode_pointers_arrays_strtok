#include <string.h>
#include <stdio.h>

char* my_strtok(char *str, char *c){

	if(!c)
		return NULL;

	static char *buffer = NULL;

	if(str){
		buffer = str;
	}else{
		if(!buffer || !*buffer){
			return NULL;
		}
	}

	char *token = buffer;
	int i = 0;
	int len = strlen(c);

	while(*buffer && i != len){
		i = 0;	//correction, not in original youtube video
		while(*buffer && *buffer != *c){
			buffer += 1;
		}
		while(buffer[i] == c[i] && i < len){
			i += 1;
		}
		if(i == len){
			*buffer = 0;
			buffer += len;
		}else{
			buffer += 1;
		}
	}

	return token;

}//my_strtok*/

int main(){

	char str[100] = "This is a meSSTOage STOPI am putting that word at the end of each sentenceSTOPIsn't it?STOP";
	char *token = 0;

	token = my_strtok(str, "STOP");
	while(token){
		printf("%s\n", token);
		token = my_strtok(NULL, "STOP");
	}

	//printf("Str is now %s\n", str);

	return 0;

}//main*/
